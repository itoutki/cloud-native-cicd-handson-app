package com.example.cicdapp;

import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {

    @Test
    @DisplayName("3でも5でも割り切れない場合、入力した数値を文字列として返すこと")
    public void test01() {
        FizzBuzz fizzbuzz = new FizzBuzz();
        assertEquals("1", fizzbuzz.fizzbuzz(1));
    }

    @Test
    @DisplayName("3で割り切れて5で割り切れない場合、fizzを返すこと")
    public void test02() {
        FizzBuzz fizzbuzz = new FizzBuzz();
        assertEquals("fizz", fizzbuzz.fizzbuzz(3));
    }

    @Test
    @DisplayName("5で割り切れて3で割り切れない場合、buzzを返すこと")
    public void test03() {
        FizzBuzz fizzbuzz = new FizzBuzz();
        assertEquals("buzz", fizzbuzz.fizzbuzz(5));

    }

    @Test
    @DisplayName("3でも5でも割り切れる場合、fizzbuzzを返すこと")
    public void test04() {
        FizzBuzz fizzbuzz = new FizzBuzz();
        assertEquals("fizzbuzz", fizzbuzz.fizzbuzz(15));

    }
}
