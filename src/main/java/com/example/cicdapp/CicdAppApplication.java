package com.example.cicdapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class CicdAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CicdAppApplication.class, args);
	}

	@GetMapping("/")
	public String hello() {
		return "Hello Cloud Native CI CD Hands-on!";
	}

	@GetMapping("/fizzbuzz")
	public String fizzbuzz(@RequestParam("number") int number) {
		FizzBuzz fizzbuzz = new FizzBuzz();
		return fizzbuzz.fizzbuzz(number);
	}

}
